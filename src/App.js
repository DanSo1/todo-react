import React, { Component } from 'react'

class App extends Component {
  state = {
    todos: []
  };
  
  componentDidMount() {
    this.getTodos();
  }

  getTodos() {
    fetch('http://127.0.0.1:8000/api/')
    .then(response => response.json())
    .then(data => this.setState({ todos: data}))
  }

  render(){
    return (
      <div>
        {this.state.todos.map(item => (
          <div key = {item.id}>
            <h1>{item.title}</h1>
            <p>{item.body}</p>
          </div>
        ))}
      </div>
    );
  }
}

export default App;
